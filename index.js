const { 
	SecretsManagerClient, 
	GetSecretValueCommand 
} = require('@aws-sdk/client-secrets-manager');

// Create secrets manager client
const secretsManagerClient = new SecretsManagerClient({
  region: process.env.AWS_DEFAULT_REGION
});

// Get secrets manager value
const secretCommand = new GetSecretValueCommand({
  SecretId: process.env.AWS_SECRET_ID
});

secretsManagerClient.send(secretCommand)
.then((data) => {
	// Process data
	let secret, decodedBinarySecret;
	if ('SecretString' in data) {
		secret = JSON.parse(data.SecretString);
		console.log(secret);
	} else {
		let buffer = new Buffer(data.SecretBinary, 'base64');
		decodedBinarySecret = buffer.toString('ascii');
		console.log(decodedBinarySecret);
	}
})
.catch((error) => {
	// Error handling
	console.warn(error);
})
.finally(() => {
	// Clean environment
	secretsManagerClient.destroy();
});